# WT MIPS

Repository for APSIM sugarcane modelling of WT_MIP demo sites.
Includes miscelaneous R code.

APSIM can be downloaded from www.apsim.info

Owner: Brendan Power brendan.power@dnrme.qld.gov.au

#R scripts

getMet.R - reads met data from xlsx file recieved from Terrain, infills sus data from SILO  data and writes APSIM met file
APSIM_plots.R - Plots APSIM output

#APSIM files

MIPS_Tully.apsim - MIPS demo site industry stanadard 



# Notes 
added additional climate and in replaced suspect data using SILO data (https://www.longpaddock.qld.gov.au/silo/about/) 
No radiation (or pan evaporation) so SILO used.
No initial soil water so I used a runoff day and set Soil water to 100% capacity on those days
Initial N – used P2R values
Currently no runoff data available. 
Yet to implemented the pesticide model.
From Alicia,  Tully sites ran off 6 Dec 2018 and Johnstone ran off 8th December





