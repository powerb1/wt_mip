#APSIM time series aggregator for Johnston Tully
# Fast because it loads all APSIM output foreach SC into memory in 1 pass.

#rm(list = ls())
suppressMessages(source("R/Functions.R"))

#plan(multiprocess,workers=6)
#nbrOfWorkers()

outDir <- "JT_TS/PEST/2020/"
#FarmFiles <- dir(outDir,"*.csv")
#FarmFiles <- dir("/scratch/apsim/wt_mip/Farm_output/2020/")

# get constDT
constDT <- get_constDT(pesticides)


# load spatial data
(load("output/JT_SptData.RData"))
JT_dt[,polygonID:=rownames(JT_dt)]
AREAS <- JT_dt[,.(polygonID,AREA)]
RepReg_DT <- unique(fread("NewJT_IntSCFU/JT_SCs_RepReg.csv"))
setnames(RepReg_DT,"Subcatchme","SUBCATS")
setnames(DAFmgt,"Region","RepReg")

# add RepReg
JT_dt <- merge(JT_dt,RepReg_DT,by="SUBCATS",all = F)

# add DAF mgt
JT_dt <- merge(JT_dt,DAFmgt,by="RepReg",all = T,allow.cartesian = T)

# add Farm files
JT_dt[,FarmFiles:=paste0(paste(APSIMSoil,APSIMClim,P2RClass,APSIMPerm,sep="$"),".csv")]
#write_lines(u(JT_dt$FarmFiles),"mips_FarmFiles.txt")
#all(unique(JT_dt$FarmFiles) %in% FarmFiles)

# add out dir
JT_dt[,outDir:=recode(RepReg,
                      Johnstone ="Farm_output/2020/",
                      Tully = "Farm_output/2020/")]

# APSIM output 
FarmFiles <- unlist(lapply(unique(JT_dt$outDir),dir,pattern=".*csv",full.names=F))
FarmFilesDT <- as.data.table(str_split(FarmFiles,"\\$|\\.",simplify = T))[,1:4]
setnames(FarmFilesDT,c("APSIMSoil","APSIMClim","SIMNAME","APSIMPerm"))
FarmFilesDT[,FarmFiles:=unlist(lapply(unique(JT_dt$outDir),dir,pattern=".*csv",full.names=T))]

# check for files
#missing <- setdiff(u(paste0(JT_dt$outDir,JT_dt$FarmFiles)),FarmFilesDT$FarmFiles)

# foreach SC load all corresponding output 
region <- "WetTropics"
SCs <- unique(JT_dt$SUBCATS)

#x1 <- dir(outDir)
#SCs <- setdiff(SCs,unique(str_split(x1,"\\$",simplify = T)[,1]))
#JT_dt[,length(unique(FarmFiles)),by="SUBCATS"][which.min(V1),]

parseSC_args <- list("AREAS" = AREAS,
                     "SOILNUT" = SOILNUT,
                     "pesticides" = pesticides,
                     "pestScen"=pestScen,
                     "region" = region,
                     "outDir" = outDir,
                     #"FarmDir" = FarmDir,
                     "FarmFiles" = FarmFiles,
                     "APSIM_slope" = APSIMparams[Region==region,slope],
                     "APSIM_slopeLength" = APSIMparams[Region==region,slope_length],
                     "APSIM_k_factor" = APSIMparams[Region==region,k_factor],
                     "constDT" = constDT,
                     "constituents"=c(SOILNUT,pesticides))

#x1 <- lapply(SCs,FastParseSC,JT_dt,parseSC_args=parseSC_args)
x1 <- mclapply(SCs,FastParseSC,JT_dt,parseSC_args=parseSC_args,mc.cores = 12)









