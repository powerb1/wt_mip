
#gets all necessary met files listed in scenario.csv

#suppressMessages(source("RScripts/Functions.R"))

region <- "Herbert"
scenarios <- read.csv(paste0("../primary/RegionsRC9/",region,"/Scenarios.csv"),as.is = T)

metCodes <- unique(scenarios$MetCode)

#metDir <-"RegionsRC9/Met/" 
#metFiles <- dir(metDir,pattern = "met$")
#all(metCodes %in% gsub(".met","",metFiles))

# API key from  https://silo.longpaddock.qld.gov.au/my-api-keys
APIkey <- "YUbqBeacerk90sW2pOfEuYhHazkQqykfWAXjWb4p"

yesterday <- Sys.Date()-1

# updates met files from SILO 
for (metCode in metCodes) {
  #metCode <- metCodes[1]
  print(metCode)
  lat <- as.numeric(paste0("-",substr(metCode,1,2),".",substr(metCode,3,4)))
  lon <- as.numeric(paste0(substr(metCode,6,8),".",substr(metCode,9,10)))
  
  
  api_url = 'https://www.longpaddock.qld.gov.au/cgi-bin/silo'

  
  url1 <- paste0("https://siloapi.longpaddock.qld.gov.au/pointdata?apikey=",APIkey,
                 "&start=18890101&finish=",format(yesterday,"%Y%m%d"),"&lat=",
                 lat,"&lon=",lon,"&format=APSIM")
  
  metFile <- paste0(metCode,".met")
  writeLines(readLines(url1),paste0(metDir,metFile))
}

https://www.longpaddock.qld.gov.au/cgi-bin/silo/PatchedPointDataset.php?start=20110101&finish=20110110&station=31010&format=json&comment=XN&username=<email_address>