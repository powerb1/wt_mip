library(tidyverse)
library(data.table)

APSIM <- fread("APSIM/Industry standard.out",skip=4)
setnames(APSIM,names(fread("APSIM/Industry standard.out",skip=2,nrows = 0)))

APSIM[,Date:=as.Date(Date,"%d/%m/%Y")]

# runoff

ggplot(APSIM,aes(Date,runoff)) +
  geom_line() + 
  labs(x = "", y="Runoff (mm)",title = "Tully Demo site - Industry standard") +
  theme_bw()
ggsave("MIPS Tully runoff.png")
  
# leaching
ggplot(APSIM,aes(Date,drain)) +
  geom_line() + 
  labs(x = "", y="Drainage (mm)",title = "Tully Demo site - Industry standard") +
  theme_bw()
ggsave("MIPS Tully drainage.png")


ggplot(APSIM,aes(Date,soil_loss)) +
  geom_line() + 
  labs(x = "", y="Soil loss (t/ha)",title = "Tully Demo site - Industry standard") +
  theme_bw()
ggsave("MIPS Tully soil loss")

APSIM$soil_loss

# biomass
ggplot(APSIM,aes(Date,cane_wt)) +
  geom_line() + 
  labs(x = "", y="Cane weight (kg/ha)",title = "Tully Demo site - Industry standard") +
  theme_bw()

