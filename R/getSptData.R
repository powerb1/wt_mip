# load spatial data from shapefiles
library(data.table)
library(readxl)
library(plyr)
library(rgdal)
#library(raster)
#library(rgeos)

JTshp <- readOGR(dsn = "NewJT_IntSCFU/JT_IntSCFU_FuncOrders_Cane",
                 layer = "JT_IntSCFU_FuncOrders_Cane",
                 stringsAsFactors = F)



JT_dt <- as.data.table(JTshp@data[,c("IntSCFU","IntSCs","APSIMSoil","APSIMPerm","APSIMClim","SUBCATS","AREA","SLOPE","KFACTSI")])

# JTshp <- readOGR(dsn = "NewJT_IntSCFU",
#                  layer = "ExportedCatchments-FUs Intersection",
#                  stringsAsFactors = F)


# WT_RC9shp <- readOGR(dsn = "D:/P2R/DAFmanagementData/WetTropics/fromRob",
#                      layer = "WT_APSIM_RC9_WithProjs",
#                      stringsAsFactors = F)

# DAF Management #######################
DAFmgt <- as.data.table(read_xlsx("D:/P2R/DAFmanagementData/WetTropics/from Rob for Baihua/WT_2018 SIM dis.xlsx",
                                  sheet = "Transfer"))
DAFmgt <- DAFmgt[Region %in% c("Tully","Johnstone")]

save(JT_dt,DAFmgt,file = "output/JT_SptData.RData",compress = T)

